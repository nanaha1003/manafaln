import torch
from torch.nn.modules.loss import _Loss
from monai.networks.utils import one_hot


class SymmetricCrossEntropyLoss(_Loss):
    def __init__(self, alpha: float, beta: float) -> None:
        super().__init__()

        self.alpha = alpha
        self.beta = beta
        self.ce_loss_fn = torch.nn.CrossEntropyLoss()

    def rce_loss_fn(
        self,
        input: torch.Tensor,
        target: torch.Tensor
    ) -> torch.Tensor:
        input = F.softmax(input, dim=1)
        input = torch.clamp(input, min=1e-7, max=1.0)

        num_classes = input.shape[1]
        target = one_hot(target, num_classes, dim=1)
        target = torch.clamp(target, min=1e-4, max=1.0)

        rce = -1.0 * torch.sum(pred * torch.log(target), dim=1)
        return rce

    def forward(
        self,
        input: torch.Tensor,
        target: torch.Tensor
    ) -> torch.Tensor:
        ce = self.ce_loss_fn(input, target)
        rce = self.rce_loss_fn(input, target)

        loss = self.alpha * ce + self.beta * rce
        return loss
