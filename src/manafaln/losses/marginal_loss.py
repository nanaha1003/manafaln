import torch
import torch.nn.functional as F
from torch.nn.modules.loss import _Loss


class MarginalLoss(_Loss):
    def __init__(
        self,
        loss: _Loss,
        foregrounds: Sequence[int],
        backgrounds: Sequence[int],
        softmax: bool = False,
        channel_dim: int = 1
    ) -> None:
        super().__init__()

        self.loss = loss
        self.foregrounds = foregrounds
        self.backgrounds = backgrounds
        self.softmax = softmax
        self.channel_dim = channel_dim

    def marginal_transform(
        self,
        data: Sequence[torch.Tensor],
        device: torch.device
    ) -> torch.Tensor:
        backgrounds = torch.index_select(
            data,
            dim=self.channel_dim,
            index=torch.tensor(self.backgrounds, device=device)
        )
        foregrounds = torch.index_select(
            data,
            dim=self.channel_dim,
            index=torch.tensor(self.foregrounds, device=device)
        )

        # Merge background channels
        backgrounds = torch.sum(backgrounds, dim=self.channel_dim, keepdim=True)

        # Add foreground channels back
        out = torch.cat([backgrounds, foregrounds], dim=self.channel_dim)
        return out

    def forward(
        self,
        input: torch.Tensor,
        targets: torch.Tensor,
        **kwargs
    ) -> torch.Tensor:
        if self.softmax:
            input = F.softmax(input, self.channel_dim)

        input = self.marginal_transform(input, input.device)
        targets = self.marginal_transform(targets, targets.device)

        return self.loss(input, targets, **kwargs)
