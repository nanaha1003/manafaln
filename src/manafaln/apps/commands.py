from typing import Dict, Optional

import typer
from lightning import Trainer
from monai.utils import set_determinism

from manafaln.apps.utils import (
    build_callbacks,
    build_data_module,
    build_logger,
    build_workflow,
)
from manafaln.core.configurators import TrainConfigurator


app = typer.Typer()

@app.command()
def train(config: str = None, ckpt: Optional[str] = None, seed: Optional[int] = None):
    pass

@app.command()
def validate():
    pass

@app.command()
def test():
    pass

@app.command()
def predict():
    pass

if __name__ == "__main__":
    app()
