import torch
from typing import Callable, Optional
from monai.losses import DeepSupervisionLoss, DiceCELoss

class DsDiceCELoss(DeepSupervisionLoss):
    def __init__(self, *args, **kwargs):
        super().__init__(DiceCELoss(*args, **kwargs))

    def __call__(self, pred, label):
        pred = [pred[:, i, ...] for i in range(pred.shape[1])]
        return super().__call__(pred, label)
