from projectmonai/monai:1.3.2

COPY . /opt/manafaln
RUN pip install --no-cache-dir /opt/manafaln && \
    rm -rf /workspace/* && \
    cp -r /opt/manafaln/examples /workspace/examples
